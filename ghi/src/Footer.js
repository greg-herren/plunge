import React from 'react';
import { Container, Row, Col, Modal } from 'react-bootstrap';
import { useState } from 'react';
import { Link } from 'react-router-dom';

function Footer() {
	const [show, setShow] = useState(false);
	return (
		<>
			<footer className="footer">
				<Container>
					<Row className="align-items-center">
						<Col sm={6} className="text-center text-sm-start align-self-center pb-2">
							<span>© 2023 Greg Herren. All Rights Reserved</span>
						</Col>
						<Col sm={6} className="text-center text-sm-end pb-2">
							<div>
								<button className="btn btn-primary my-2" onClick={() => setShow(true)}><span>Peek under the hood</span></button>
							</div>
						</Col>
					</Row>
				</Container>
			</footer>
			<Modal show={show} onHide={() => {setShow(false)}}>
				<Modal.Header closeButton className="under-hood-modal-header">
					<Modal.Title>
						Under the hood
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Container>
						<Row>
							<Col xs={12}>
								<p>Plunge was originally a collaboration between me &#40;
									<Link
										to="https://www.linkedin.com/in/greg-herren/"
										target="_blank"
										rel="noreferrer">
										Greg Herren
									</Link>&#41;,&nbsp;
									<Link
										to="https://www.linkedin.com/in/hnrykm/"
										target="_blank"
										rel="noreferrer">
										Henry Kim
									</Link>,
									&nbsp;
									<Link
										to="https://www.linkedin.com/in/simon-conrad/"
										target="_blank"
										rel="noreferrer">
											Simon Conrad
									</Link>, and&nbsp;
									<Link
										to="https://www.linkedin.com/in/taras-semeniv-402045259/"
										target="_blank"
										rel="noreferrer">
										Travis Semeniv
									</Link>&nbsp;as the final project for a Hack Reactor bootcamp. Since then, I've forked the repo and continued development independently.
								</p>
								<p>Plunge was built using:</p>
								<Row className="d-flex justify-content-center">
									<Col className="m-1" sm={1}>
										<Link
											to="https://www.javascript.com/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/javascript.png"
												alt="Logo"
												height="31px"
												title="JavaScript"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://www.python.org/"
											target="_blank"
											rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/python.png"
												alt="Logo"
												height="31px"
												title="Python 3"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://html.spec.whatwg.org/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/html5.png"
												alt="Logo"
												height="31px"
												title="HTML 5"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://www.w3.org/TR/CSS/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/css3.png"
												alt="Logo"
												height="31px"
												title="CSS 3"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://react.dev/"
											target="_blank"
											rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/react.png"
												alt="Logo"
												height="31px"
												title="React.js"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://fastapi.tiangolo.com/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/fastapi.png"
												alt="Logo"
												height="31px"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://redux.js.org/"
											target="_blank"
											rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/redux.png"
												alt="Logo"
												height="31px"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://getbootstrap.com/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/bootstrap.png"
												alt="Logo"
												height="31px"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://www.postgresql.org/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/postgresql.png"
												alt="Logo"
												height="31px"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://www.docker.com/"
											target="_blank"
											rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/docker.png"
												alt="Logo"
												height="31px"
												title="Docker"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link to="https://gitlab.com/" target="_blank" rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/gitlab.png"
												alt="Logo"
												height="31px"
												title="Gitlab"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://code.visualstudio.com/"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/vscode.png"
												alt="Logo"
												height="31px"
												title="Visual Studio Code"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://linear.app/"
											target="_blank"
											rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/linear.png"
												alt="Logo"
												height="31px"
												title="Linear"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://slack.com/"
											target="_blank"
											rel="noreferrer">
											<img
												src="https://henrykimphotography.com/plunge/slack.png"
												alt="Logo"
												height="31px"
												title="Slack"
											/>
										</Link>
									</Col>
									<Col className="m-1" sm={1}>
										<Link
											to="https://developers.google.com/maps"
											target="_blank"
											rel="noreferrer"
										>
											<img
												src="https://henrykimphotography.com/plunge/googlemaps.png"
												alt="Logo"
												height="31px"
												title="Google Maps API"
											/>
										</Link>
									</Col>
								</Row>
							</Col>
						</Row>
					</Container>
				</Modal.Body>
				<Modal.Footer>
					<button className="btn btn-secondary m-1" onClick = {() => {window.open("https://hey-greg.com/", '_blank')}}>View my Portfolio</button>
					<button className="btn btn-primary m-1" onClick = {() => {window.open("https://gitlab.com/greg-herren/plunge", '_blank')}}>View Code</button>
				</Modal.Footer>
			</Modal>
		</>
	)
}

export default Footer;
